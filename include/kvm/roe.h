/* SPDX-License-Identifier: GPL-2.0 */

#ifndef __KVM_ROE_H__
#define __KVM_ROE_H__
/*
 * KVM Read Only Enforcement
 * Copyright (c) 2018 Ahmed Abd El Mawgood
 *
 * Author Ahmed Abd El Mawgood <ahmedsoliman@mena.vt.edu>
 *
 */
void kvm_roe_arch_commit_protection(struct kvm *kvm,
		struct kvm_memory_slot *slot);
int kvm_roe(struct kvm_vcpu *vcpu, u64 a0, u64 a1, u64 a2, u64 a3);
bool kvm_roe_arch_is_userspace(struct kvm_vcpu *vcpu);
bool kvm_roe_check_range(struct kvm_memory_slot *slot, gfn_t gfn, int offset,
		int len);
static inline bool gfn_is_full_roe(struct kvm_memory_slot *slot, gfn_t gfn)
{
	return test_bit(gfn - slot->base_gfn, slot->roe_bitmap);

}
static inline bool gfn_is_partial_roe(struct kvm_memory_slot *slot, gfn_t gfn)
{
	return test_bit(gfn - slot->base_gfn, slot->partial_roe_bitmap);
}

#endif
