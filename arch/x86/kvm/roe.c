// SPDX-License-Identifier: GPL-2.0

/*
 * KVM Read Only Enforcement
 * Copyright (c) 2018 Ahmed Abd El Mawgood
 *
 * Author: Ahmed Abd El Mawgood <ahmedsoliman@mena.vt.edu>
 *
 */
#include <linux/types.h>
#include <linux/kvm_host.h>
#include <kvm/roe.h>


#include <asm/kvm_host.h>
#include "kvm_cache_regs.h"
#include "mmu.h"
#include "roe_arch.h"

static bool __rmap_write_protect_roe(struct kvm *kvm,
		struct kvm_rmap_head *rmap_head, bool pt_protect,
		struct kvm_memory_slot *memslot)
{
	u64 *sptep;
	struct rmap_iterator iter;
	bool prot;
	bool flush = false;
	void *full_bmp =  memslot->roe_bitmap;
	void *part_bmp = memslot->partial_roe_bitmap;

	for_each_rmap_spte(rmap_head, &iter, sptep) {
		int idx = spte_to_gfn(sptep) - memslot->base_gfn;

		prot = !(test_bit(idx, full_bmp) || test_bit(idx, part_bmp));
		prot = prot && pt_protect;
		flush |= spte_write_protect(sptep, prot);
	}
	return flush;
}

bool kvm_mmu_slot_gfn_write_protect_roe(struct kvm *kvm,
		struct kvm_memory_slot *slot, u64 gfn)
{
	struct kvm_rmap_head *rmap_head;
	int i;
	bool write_protected = false;

	for (i = PT_PAGE_TABLE_LEVEL; i <= PT_MAX_HUGEPAGE_LEVEL; ++i) {
		rmap_head = __gfn_to_rmap(gfn, i, slot);
		write_protected |= __rmap_write_protect_roe(kvm, rmap_head,
				true, slot);
	}
	return write_protected;
}

static bool slot_rmap_apply_protection(struct kvm *kvm,
		struct kvm_rmap_head *rmap_head, void *data)
{
	struct kvm_memory_slot *memslot = (struct kvm_memory_slot *) data;
	bool prot_mask = !(memslot->flags & KVM_MEM_READONLY);

	return __rmap_write_protect_roe(kvm, rmap_head, prot_mask, memslot);
}

bool roe_protect_all_levels(struct kvm *kvm, struct kvm_memory_slot *memslot)
{
	bool flush;

	spin_lock(&kvm->mmu_lock);
	flush = slot_handle_all_level(kvm, memslot, slot_rmap_apply_protection,
			false, memslot);
	spin_unlock(&kvm->mmu_lock);
	return flush;
}

void kvm_roe_arch_commit_protection(struct kvm *kvm,
		struct kvm_memory_slot *slot)
{
	kvm_mmu_slot_apply_write_access(kvm, slot);
	kvm_arch_flush_shadow_memslot(kvm, slot);
}
EXPORT_SYMBOL_GPL(kvm_roe_arch_commit_protection);

bool kvm_roe_arch_is_userspace(struct kvm_vcpu *vcpu)
{
	u64 rflags;
	u64 cr0 = kvm_read_cr0(vcpu);
	u64 iopl;

	// first checking we are not in protected mode
	if ((cr0 & 1) == 0)
		return false;
	/*
	 * we don't need to worry about comments in __get_regs
	 * because we are sure that this function will only be
	 * triggered at the end of a hypercall instruction.
	 */
	rflags = kvm_get_rflags(vcpu);
	iopl = (rflags >> 12) & 3;
	if (iopl != 3)
		return false;
	return true;
}
EXPORT_SYMBOL_GPL(kvm_roe_arch_is_userspace);
