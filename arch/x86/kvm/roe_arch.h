/* SPDX-License-Identifier: GPL-2.0 */

#ifndef __KVM_ROE_HARCH_H__
#define __KVM_ROE_HARCH_H__
/*
 * KVM Read Only Enforcement
 * Copyright (c) 2018 Ahmed Abd El Mawgood
 *
 * Author: Ahmed Abd El Mawgood <ahmedsoliman@mena.vt.edu>
 *
 */
#include "mmu.h"

bool roe_protect_all_levels(struct kvm *kvm, struct kvm_memory_slot *memslot);

static inline bool protect_all_levels(struct kvm *kvm,
		struct kvm_memory_slot *memslot)
{
	return roe_protect_all_levels(kvm, memslot);
}
bool kvm_mmu_slot_gfn_write_protect_roe(struct kvm *kvm,
		struct kvm_memory_slot *slot, u64 gfn);
static inline bool kvm_mmu_slot_gfn_write_protect(struct kvm *kvm,
		struct kvm_memory_slot *slot, u64 gfn)
{
	return kvm_mmu_slot_gfn_write_protect_roe(kvm, slot, gfn);
}
#endif
