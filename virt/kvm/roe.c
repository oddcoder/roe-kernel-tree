// SPDX-License-Identifier: GPL-2.0

/*
 * KVM Read Only Enforcement
 * Copyright (c) 2018 Ahmed Abd El Mawgood
 *
 * Author: Ahmed Abd El Mawgood <ahmedsoliman@mena.vt.edu>
 *
 */
#include <linux/kvm_host.h>
#include <linux/kvm.h>
#include <linux/kvm_para.h>
#include <kvm/roe.h>
#include "roe_generic.h"

int kvm_roe_init(struct kvm_memory_slot *slot)
{
	slot->roe_bitmap = kvzalloc(BITS_TO_LONGS(slot->npages) *
			sizeof(unsigned long), GFP_KERNEL);
	if (!slot->roe_bitmap)
		goto fail1;
	slot->partial_roe_bitmap = kvzalloc(BITS_TO_LONGS(slot->npages) *
			sizeof(unsigned long), GFP_KERNEL);
	if (!slot->partial_roe_bitmap)
		goto fail2;
	slot->prot_root = kvzalloc(sizeof(struct rb_root), GFP_KERNEL);
	if (!slot->prot_root)
		goto fail3;
	*slot->prot_root = RB_ROOT;
	return 0;
fail3:
	kvfree(slot->partial_roe_bitmap);
fail2:
	kvfree(slot->roe_bitmap);
fail1:
	return -ENOMEM;

}

static bool kvm_roe_protected_range(struct kvm_memory_slot *slot, gpa_t gpa,
		int len)
{
	struct rb_node *node = slot->prot_root->rb_node;

	while (node) {
		struct protected_chunk *cur_chunk;
		int cmp;

		cur_chunk = rb_entry(node, struct protected_chunk, node);
		cmp = kvm_roe_range_cmp_position(cur_chunk, gpa, len);
		if (cmp < 0)/*target chunk is before current node*/
			node = node->rb_left;
		else if (cmp > 0)/*target chunk is after current node*/
			node = node->rb_right;
		else
			return true;
	}
	return false;
}

bool kvm_roe_check_range(struct kvm_memory_slot *slot, gfn_t gfn, int offset,
		int len)
{
	gpa_t gpa = (gfn << PAGE_SHIFT) + offset;

	if (!gfn_is_partial_roe(slot, gfn))
		return false;
	return kvm_roe_protected_range(slot, gpa, len);
}
EXPORT_SYMBOL_GPL(kvm_roe_check_range);

static void kvm_roe_destroy_tree(struct rb_node *node)
{
	struct protected_chunk *cur_chunk;

	if (!node)
		return;
	kvm_roe_destroy_tree(node->rb_left);
	kvm_roe_destroy_tree(node->rb_right);
	cur_chunk = rb_entry(node, struct protected_chunk, node);
	kvfree(cur_chunk);
}

void kvm_roe_free(struct kvm_memory_slot *slot)
{
	kvfree(slot->roe_bitmap);
	kvfree(slot->partial_roe_bitmap);
	kvm_roe_destroy_tree(slot->prot_root->rb_node);
	kvfree(slot->prot_root);
}

static void kvm_warning_roe_violation(u64 addr, const void *data, int len)
{
	int i;
	const char *d = data;
	char *buf = kvmalloc(len * 3 + 1, GFP_KERNEL);

	for (i = 0; i < len; i++)
		sprintf(buf+3*i, " %02x", d[i]);
	pr_warn("ROE violation:\n");
	pr_warn("\tAttempt to write %d bytes at address 0x%08llx\n", len, addr);
	pr_warn("\tData: %s\n", buf);
	kvfree(buf);
}

void kvm_roe_check_and_log(struct kvm_memory_slot *memslot, gfn_t gfn,
		const void *data, int offset, int len)
{
	if (!memslot)
		return;
	if (!gfn_is_full_roe(memslot, gfn) &&
		!kvm_roe_check_range(memslot, gfn, offset, len))
		return;
	kvm_warning_roe_violation((gfn << PAGE_SHIFT) + offset, data, len);
}

static void kvm_roe_protect_slot(struct kvm *kvm, struct kvm_memory_slot *slot,
		gfn_t gfn, u64 npages, bool partial)
{
	int i;
	void *bitmap;

	if (partial)
		bitmap = slot->partial_roe_bitmap;
	else
		bitmap = slot->roe_bitmap;
	for (i = gfn - slot->base_gfn; i < gfn + npages - slot->base_gfn; i++)
		set_bit(i, bitmap);
	kvm_roe_arch_commit_protection(kvm, slot);
}


static int __kvm_roe_protect_range(struct kvm *kvm, gpa_t gpa, u64 npages,
		bool partial)
{
	struct kvm_memory_slot *slot;
	gfn_t gfn = gpa >> PAGE_SHIFT;
	int count = 0;

	while (npages != 0) {
		slot = gfn_to_memslot(kvm, gfn);
		if (!slot) {
			gfn += 1;
			npages -= 1;
			continue;
		}
		if (gfn + npages > slot->base_gfn + slot->npages) {
			u64 _npages = slot->base_gfn + slot->npages - gfn;

			kvm_roe_protect_slot(kvm, slot, gfn, _npages, partial);
			gfn += _npages;
			count += _npages;
			npages -= _npages;
		} else {
			kvm_roe_protect_slot(kvm, slot, gfn, npages, partial);
			count += npages;
			npages = 0;
		}
	}
	if (count == 0)
		return -EINVAL;
	return count;
}

static int kvm_roe_protect_range(struct kvm *kvm, gpa_t gpa, u64 npages,
		bool partial)
{
	int r;

	mutex_lock(&kvm->slots_lock);
	r = __kvm_roe_protect_range(kvm, gpa, npages, partial);
	mutex_unlock(&kvm->slots_lock);
	return r;
}


static int kvm_roe_full_protect_range(struct kvm_vcpu *vcpu, u64 gva,
		u64 npages)
{
	struct kvm *kvm = vcpu->kvm;
	gpa_t gpa;
	u64 hva;
	u64 count = 0;
	int i;
	int status;

	if (gva & ~PAGE_MASK)
		return -EINVAL;
	// We need to make sure that there will be no overflow
	if ((npages << PAGE_SHIFT) >> PAGE_SHIFT != npages || npages == 0)
		return -EINVAL;
	for (i = 0; i < npages; i++) {
		gpa = kvm_mmu_gva_to_gpa_system(vcpu, gva + (i << PAGE_SHIFT),
				NULL);
		hva = gfn_to_hva(kvm, gpa >> PAGE_SHIFT);
		if (kvm_is_error_hva(hva))
			continue;
		if (!access_ok(hva, 1 << PAGE_SHIFT))
			continue;
		status =  kvm_roe_protect_range(vcpu->kvm, gpa, 1, false);
		if (status > 0)
			count += status;
	}
	if (count == 0)
		return -EINVAL;
	return count;
}

static u64 kvm_roe_expand_chunk(struct protected_chunk *pos, u64 gpa, u64 size)
{
	u64 old_ptr = pos->gpa;
	u64 old_size = pos->size;
	u64 ret = 0;

	if (gpa < old_ptr) {
		pos->gpa = gpa;
		ret |= KVM_ROE_MERGE_LEFT;
	}
	if (gpa + size > old_ptr + old_size) {
		pos->size = gpa + size - pos->gpa;
		ret |= KVM_ROE_MERGE_RIGHT;
	}
	return ret;
}
static void kvm_roe_merge_left(struct rb_root *root, struct rb_node *start)
{
	struct rb_root fake_root;
	struct protected_chunk *target, *first;
	struct rb_node *node, *stop;
	u64 i, count = 0;

	if (!start->rb_left)
		return;
	fake_root = (struct rb_root) {start->rb_left};
	stop = rb_prev(rb_first(&fake_root));
	/* Back traverse till no node can be merged*/
	target = container_of(start, struct protected_chunk, node);
	for (node = rb_last(&fake_root); node != stop; node = rb_prev(node)) {
		struct protected_chunk *pos;

		pos = container_of(node, struct protected_chunk, node);
		if (kvm_roe_range_cmp_mergability(target, pos->gpa, pos->size))
			break;
		count += 1;
	}
	if (!count)
		return;
	/* merging step*/
	node = rb_next(node);
	first = container_of(node, struct protected_chunk, node);
	kvm_roe_expand_chunk(target, first->gpa, first->size);
	/* forward traverse and delete all in between*/
	for (i = 0; i < count; i++) {
		struct protected_chunk *pos;

		pos = container_of(node, struct protected_chunk, node);
		rb_erase(node, root);
		kvfree(pos);
		node = rb_next(node);
	}
}

static void kvm_roe_merge_right(struct rb_root *root, struct rb_node *start)
{
	struct rb_root fake_root;
	struct protected_chunk *target, *first;
	struct rb_node *node, *stop;
	u64 i, count = 0;

	if (!start->rb_right)
		return;
	fake_root = (struct rb_root) {start->rb_right};
	stop = rb_next(rb_last(&fake_root));
	/* Forward traverse till no node can be merged*/
	target = container_of(start, struct protected_chunk, node);
	for (node = rb_first(&fake_root); node != stop; node = rb_next(node)) {
		struct protected_chunk *pos;

		pos = container_of(node, struct protected_chunk, node);
		if (kvm_roe_range_cmp_mergability(target, pos->gpa, pos->size))
			break;
		count += 1;
	}
	if (!count)
		return;
	/* merging step*/
	node = rb_prev(node);
	first = container_of(node, struct protected_chunk, node);
	kvm_roe_expand_chunk(target, first->gpa, first->size);
	/* Backward traverse and delete all in between*/
	for (i = 0; i < count; i++) {
		struct protected_chunk *pos;

		pos = container_of(node, struct protected_chunk, node);
		rb_erase(node, root);
		kvfree(pos);
		node = rb_prev(node);
	}
}

static bool kvm_roe_merge_chunks(struct rb_root *root, struct rb_node *target,
		u64 gpa, u64 size)
{
	/*
	 * attempt merging all adjacent chunks after inserting a chunk that is
	 * adjacent or inersecting with  an existing chunk
	 */
	struct protected_chunk *cur;
	u64 merge;

	cur = container_of(target, struct protected_chunk, node);
	merge = kvm_roe_expand_chunk(cur, gpa, size);
	/*
	 * We will not have to worry about the parent node while merging
	 * If it was mergeable with the new to be inserted chunk we wouldn't
	 * have gone deeper.
	 **/
	if (merge & KVM_ROE_MERGE_LEFT)
		kvm_roe_merge_left(root, target);
	if (merge & KVM_ROE_MERGE_RIGHT)
		kvm_roe_merge_right(root, target);
	return true;
}

static int __kvm_roe_insert_chunk(struct kvm_memory_slot *slot, u64 gpa,
		u64 size)
{
	/* kvm->slots_lock must be acquired*/
	struct rb_node **new = &(slot->prot_root->rb_node), *parent = NULL;
	struct protected_chunk *insert_me;
	bool merge = false;

	while (*new) {
		struct protected_chunk *chunk;
		int cmp;

		chunk = container_of(*new, struct protected_chunk, node);
		cmp = kvm_roe_range_cmp_mergability(chunk, gpa, size);
		parent = *new;
		if (cmp < 0) {
			new = &((*new)->rb_left);
		} else if (cmp > 0) {
			new = &((*new)->rb_right);
		} else {
			merge = true;
			kvm_roe_merge_chunks(slot->prot_root, *new, gpa, size);
			break;
		}
	}
	if (merge)
		return size;
	insert_me = kvzalloc(sizeof(struct protected_chunk), GFP_KERNEL);
	insert_me->gpa = gpa;
	insert_me->size = size;
	rb_link_node(&insert_me->node, parent, new);
	rb_insert_color(&insert_me->node, slot->prot_root);
	return size;
}

static int kvm_roe_insert_chunk(struct kvm *kvm, u64 gpa, u64 size)
{
	struct kvm_memory_slot *slot;
	gfn_t gfn = gpa >> PAGE_SHIFT;
	int ret;

	mutex_lock(&kvm->slots_lock);
	slot = gfn_to_memslot(kvm, gfn);
	ret = __kvm_roe_insert_chunk(slot, gpa, size);
	mutex_unlock(&kvm->slots_lock);
	return ret;
}

static int kvm_roe_partial_page_protect(struct kvm_vcpu *vcpu, u64 gva,
		u64 size)
{
	gpa_t gpa = kvm_mmu_gva_to_gpa_system(vcpu, gva, NULL);

	kvm_roe_protect_range(vcpu->kvm, gpa, 1, true);
	return kvm_roe_insert_chunk(vcpu->kvm, gpa, size);
}

static int kvm_roe_partial_protect(struct kvm_vcpu *vcpu, u64 gva, u64 size)
{
	u64 gva_start = gva;
	u64 gva_end = gva+size;
	u64 gpn_start = gva_start >> PAGE_SHIFT;
	u64 gpn_end = gva_end >> PAGE_SHIFT;
	u64 _size;
	int count = 0;
	// We need to make sure that there will be no overflow or zero size
	if (gva_end <= gva_start)
		return -EINVAL;

	// protect the partial page at the start
	if (gpn_end > gpn_start)
		_size = PAGE_SIZE - (gva_start & PAGE_MASK) + 1;
	else
		_size = size;
	size -= _size;
	count += kvm_roe_partial_page_protect(vcpu, gva_start, _size);
	// full protect in the middle pages
	if (gpn_end - gpn_start > 1) {
		int ret;
		u64 _gva = (gpn_start + 1) << PAGE_SHIFT;
		u64 npages = gpn_end - gpn_start - 1;

		size -= npages << PAGE_SHIFT;
		ret = kvm_roe_full_protect_range(vcpu, _gva, npages);
		if (ret > 0)
			count += ret << PAGE_SHIFT;
	}
	// protect the partial page at the end
	if (size != 0)
		count += kvm_roe_partial_page_protect(vcpu,
				gpn_end << PAGE_SHIFT, size);
	if (count == 0)
		return -EINVAL;
	return count;
}

int kvm_roe(struct kvm_vcpu *vcpu, u64 a0, u64 a1, u64 a2, u64 a3)
{
	int ret;
	/*
	 * First we need to make sure that we are running from something that
	 * isn't usermode
	 */
	if (kvm_roe_arch_is_userspace(vcpu))
		return -KVM_ENOSYS;
	switch (a0) {
	case ROE_VERSION:
		ret = 2; //current version
		break;
	case ROE_MPROTECT:
		ret = kvm_roe_full_protect_range(vcpu, a1, a2);
		break;
	case ROE_MPROTECT_CHUNK:
		ret = kvm_roe_partial_protect(vcpu, a1, a2);
		break;
	default:
		ret = -EINVAL;
	}
	return ret;
}
EXPORT_SYMBOL_GPL(kvm_roe);
