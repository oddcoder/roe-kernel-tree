/* SPDX-License-Identifier: GPL-2.0 */

#ifndef __KVM_ROE_GENERIC_H__
#define __KVM_ROE_GENERIC_H__
/*
 * KVM Read Only Enforcement
 * Copyright (c) 2018 Ahmed Abd El Mawgood
 *
 * Author Ahmed Abd El Mawgood <ahmedsoliman@mena.vt.edu>
 *
 */

#define KVM_ROE_MERGE_LEFT	0x1
#define KVM_ROE_MERGE_RIGHT	0x2

void kvm_roe_free(struct kvm_memory_slot *slot);
int kvm_roe_init(struct kvm_memory_slot *slot);
bool kvm_roe_check_range(struct kvm_memory_slot *slot, gfn_t gfn, int offset,
		int len);
void kvm_roe_check_and_log(struct kvm_memory_slot *memslot, gfn_t gfn,
		const void *data, int offset, int len);
#endif
